# test_cf_docker.py

import logging
import re
import pytest
from cheesefactory_docker import DockerContainer

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(module)-20s line:%(lineno)-4d %(levelname)-8s %(message)s'
)
logger = logging.getLogger(__name__)

CONTAINER_IMAGE = 'hello-world'
CONTAINER_NAME = 'my_container_name'


#
# Bare container tests.
#

@pytest.fixture(scope="module")
def bare_container():
    """Return a container that uses default settings.

    Returns:
        A DockerContainer object.
    """
    return DockerContainer()


def test_bare_pull_image(bare_container):
    bare_container.pull_image(CONTAINER_IMAGE)


def test_bare_run_container(bare_container):
    bare_container.run_container(
        image=CONTAINER_IMAGE,
        container_name=CONTAINER_NAME,
        detach=True,
        environment=['first_name=John', 'last_name=Doe'],
        ports={'8080': '8080'}
    )


def test_bare_engine_version(bare_container):
    version = bare_container.engine_version
    logger.debug(f'version: {version}')
    assert re.search(r'^[0-9]+\.[0-9]+\.[0-9]+$', version) is not None


#
# Pre-configured container tests.
#

@pytest.fixture(scope="module")
def configured_container():
    """Return a pre-configured container--one that does not use all defaults.

    Returns:
        A DockerContainer object.
    """
    return DockerContainer(
        image=CONTAINER_IMAGE,
        container_name=CONTAINER_NAME,
        detach=False,  # We do not want to detach from the container so we can see the logs.
    )


def test_configured_container(configured_container):
    assert re.search(r'Hello from Docker', str(configured_container.container)) is not None


def test_configured_engine_version(configured_container):
    version = configured_container.engine_version
    logger.debug(f'version: {version}')
    assert re.search(r'^[0-9]+\.[0-9]+\.[0-9]+$', version) is not None
