# cheesefactory-docker

-----------------

##### A toolset to create Docker containers and read the contents of secret and config files.
[![PyPI Latest Release](https://img.shields.io/pypi/v/cheesefactory-docker.svg)](https://pypi.org/project/cheesefactory-database/)
[![PyPI status](https://img.shields.io/pypi/status/cheesefactory-docker.svg)](https://pypi.python.org/pypi/cheesefactory-database/)
[![PyPI download month](https://img.shields.io/pypi/dm/cheesefactory-docker.svg)](https://pypi.python.org/pypi/cheesefactory-database/)
[![PyPI download week](https://img.shields.io/pypi/dw/cheesefactory-docker.svg)](https://pypi.python.org/pypi/cheesefactory-database/)
[![PyPI download day](https://img.shields.io/pypi/dd/cheesefactory-docker.svg)](https://pypi.python.org/pypi/cheesefactory-database/)

## Main Features

* Creates containers using Docker's docker-py package

**Note:** _This package is still in beta status. As such, future versions may not be backwards compatible and features may change._

## Installation
The source is hosted at https://bitbucket.org/hellsgrannies/cheesefactory-docker


```sh
pip install cheesefactory-docker
```

## Dependencies

* [docker](https://docker-py.readthedocs.io/en/stable/index.html)
  
## License

## Parameters

## Examples
